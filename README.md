# Templates

## Installation

1. Clone the repository
2. Navigate into the repository and pack the repository with `dotnet pack`
3. Install the package with `dotnet new -i {path to the template package}`

## Add new template

- Make a new project (Or use an excisting one)
- Add .template.config subfolder and a new file inside called template.json like so

```
project
└───.template.config
        template.json
```

- Open the template.json file and add this. Modify as you want.
    "sourceName" specifies which filenames should be changed when installing the template. `dotnet new {template} -n {replaces sourceName};`
    
```json
{
    "author": "Me",
    "classifications": [ "Common", "Console", "C#8" ],
    "identity": "Template.CosProject",
    "name": "Cos Project",
    "shortName": "DefaultCos",
    "tags": {
        "language": "C#",
        "type": "project"
    },
    "sourceName": "DefaultCos.csproj"
}
```

- Add the project folder to this repository in the 'Templates' folder

```
CosTemplates
└───Templates
    └───{template1}
    └───{template2}
    └───{yourTemplateProject}
```

- Push your changes and your newly added template will be included the next time you use the racr cli!
- test