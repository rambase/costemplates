﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TemplateWizard;
using System.Windows.Forms;
using EnvDTE;

namespace Wizard
{
    public class WizardImplementation : IWizard
    {
        private UserInputForm inputForm;
        private string archive;
        private string objectType;
        private string namespacestr;
        private string notfounderr;

        // This method is called before opening any item that
        // has the OpenInEditor attribute.
        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectFinishedGenerating(Project project)
        {
        }

        // This method is only called for item templates,
        // not for project templates.
        public void ProjectItemFinishedGenerating(ProjectItem
            projectItem)
        {
        }

        // This method is called after the project is created.
        public void RunFinished()
        {
        }

        public void RunStarted(object automationObject,
            Dictionary<string, string> replacementsDictionary,
            WizardRunKind runKind, object[] customParams)
        {
            try
            {
                // Display a form to the user. The form collects
                // input for the custom message.
                inputForm = new UserInputForm();
                inputForm.Text = "API : DELETE (Main)";
                inputForm.ShowDialog();

                archive = UserInputForm.Archive;
                objectType = UserInputForm.ObjectType;
                namespacestr = UserInputForm.NamespaceStr;
                notfounderr = UserInputForm.NotFoundErr;

                // Add custom parameters.
                replacementsDictionary.Add("$archive$",
                    archive);
                replacementsDictionary.Add("$objectType$",
                    objectType);
                replacementsDictionary.Add("$namespace$",
                    namespacestr);
                replacementsDictionary.Add("$notfounderr$",
                    notfounderr);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        // This method is only called for item templates,
        // not for project templates.
        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }
    }

    public partial class UserInputForm : Form
    {
        private static string archive;
        private static string objectType;
        private static string namespacestr;
        private static string notfounderr;
        private Label header;
        private TextBox textBox1;
        private Button button1;
        private Label label1;
        private Label label2;
        private ToolStripDropDownMenu dropDownMenu1;
        private TextBox textBox2;
        private Label label3;
        private TextBox textBox3;
        private Label label4;
        private TextBox textBox4;
        private Button button2;
        private Button button3;

        public UserInputForm()
        {

            this.Size = new System.Drawing.Size(350, 450);

            header = new Label();
            header.Location = new System.Drawing.Point(10, 25);
            header.Size = new System.Drawing.Size(120, 50);
            header.Text = "API : DELETE (Main)";
            this.Controls.Add(header);

            label1 = new Label();
            label1.Location = new System.Drawing.Point(10, 85);
            label1.Size = new System.Drawing.Size(70, 20);
            label1.Text = "Archieve";
            this.Controls.Add(label1);

            //dropDownMenu1 = new ToolStripDropDownMenu();
            //dropDownMenu1.TopLevel = false;
            //dropDownMenu1.Location = new System.Drawing.Point(90, 45);
            //dropDownMenu1.Size = new System.Drawing.Size(70, 20);
            //this.Controls.Add(dropDownMenu1);
            textBox1 = new TextBox();
            textBox1.Location = new System.Drawing.Point(120, 85);
            textBox1.Size = new System.Drawing.Size(150, 20);
            this.Controls.Add(textBox1);

            label2 = new Label();
            label2.Location = new System.Drawing.Point(10, 115);
            label2.Size = new System.Drawing.Size(70, 20);
            label2.Text = "Object Type";
            this.Controls.Add(label2);

            textBox2 = new TextBox();
            textBox2.Location = new System.Drawing.Point(120, 115);
            textBox2.Size = new System.Drawing.Size(150, 20);
            this.Controls.Add(textBox2);

            label3 = new Label();
            label3.Location = new System.Drawing.Point(10, 145);
            label3.Size = new System.Drawing.Size(70, 20);
            label3.Text = "Namespace";
            this.Controls.Add(label3);

            textBox3 = new TextBox();
            textBox3.Location = new System.Drawing.Point(120, 145);
            textBox3.Size = new System.Drawing.Size(150, 20);
            this.Controls.Add(textBox3);

            label4 = new Label();
            label4.Location = new System.Drawing.Point(10, 175);
            label4.Size = new System.Drawing.Size(100, 20);
            label4.Text = "Not found error";
            this.Controls.Add(label4);

            textBox4 = new TextBox();
            textBox4.Location = new System.Drawing.Point(120, 175);
            textBox4.Size = new System.Drawing.Size(150, 20);
            this.Controls.Add(textBox4);

            button1 = new Button();
            button1.Text = "Select";
            button1.Location = new System.Drawing.Point(290, 175);
            button1.Size = new System.Drawing.Size(50, 25);
            button1.Click += Select_Button_Click;
            this.Controls.Add(button1);

            button2 = new Button();
            button2.Text = "Cancel";
            button2.Location = new System.Drawing.Point(100, 235);
            button2.Size = new System.Drawing.Size(50, 25);
            button2.Click += Cancel_Button_Click;
            this.Controls.Add(button2);

            button3 = new Button();
            button3.Text = "Create COS";
            button3.Location = new System.Drawing.Point(160, 235);
            button3.Size = new System.Drawing.Size(100, 25);
            button3.Click += CreateCOS_Button_Click;
            this.Controls.Add(button3);

        }
        public static string Archive
        {
            get
            {
                return archive;
            }
            set
            {
                archive = value;
            }
        }

        public static string ObjectType
        {
            get
            {
                return objectType;
            }
            set
            {
                objectType = value;
            }
        }

        public static string NamespaceStr
        {
            get
            {
                return namespacestr;
            }
            set
            {
                namespacestr = value;
            }
        }

        public static string NotFoundErr
        {
            get
            {
                return notfounderr;
            }
            set
            {
                notfounderr = value;
            }
        }
        private void CreateCOS_Button_Click(object sender, EventArgs e)
        {
            archive = textBox1.Text;
            objectType = textBox2.Text;
            namespacestr = textBox3.Text;
            notfounderr = textBox4.Text;
            this.Close();
        }

        private void Select_Button_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}