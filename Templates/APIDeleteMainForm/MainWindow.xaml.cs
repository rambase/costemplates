﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace APIDeleteMainForm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            APIDeleteMain obj = new APIDeleteMain();
            obj.Archive = archive.Text;
            obj.ObjectType = objectType.Text;
            obj.Namespace = @namespace.Text;
            obj.NotFoundError = notfounderror.Text;

            string Path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).Replace("\\", "/") + "/racrtemp/APIDeleteMain.json";
            string output = JsonConvert.SerializeObject(obj, Formatting.Indented);
            File.WriteAllText(Path, output);

            this.Close();
        }

        //private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
            
        //}
    }

    public class APIDeleteMain
    {
        public string Archive { get; set; }
        public string ObjectType { get; set; }
        public string Namespace { get; set; }
        public string NotFoundError { get; set; }
    }
}
